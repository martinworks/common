requirejs.config({
    baseUrl: '/js',
    paths:{
    'jquery': 'lib/jquery-1.9.1.min',
    'jqueryEasing': 'lib/jquery.easing.min',
    'jqueryUI': 'lib/jquery-ui',

		//Function Module
        'responsive':'module/reponsive',
		'$ModulePop' : 'module/$ModulePop',
		'ModulePop' : 'module/ModulePop',
		'ModuleMobileScroll' : 'module/ModuleMobileScroll',
		'$ModuleMobileScroll' : 'module/$ModuleMobileScroll',
		'$SelectBox' : 'module/$SelectBox',
		'SelectBox' : 'module/SelectBox',
        '$tab':'module/$tab',
        'tab':'module/tab',
        '$number':'module/number',
        '$moveTop':'module/$moduleMoveTop'
    },
    shim:{
        'jquery':{
            exports:'$'
        },
		'jqueryUI':{
			deps: ['jquery'],
			exports :'jqueryUI'
		},
		'jqueryEasing':{
			deps: ['jquery'],
			exports :'jqueryEasing'
		}
    }
});

define(['jquery', 'jqueryEasing'],function ($, easing) {
        /****** Check PC/Tablet/Mobile Device size ******/
        //require(["responsive"]);
		/****** Module SelectBox with Jquery ******/
		require(["$SelectBox"], function(SelectBox) {
            var element = $("[data-module-pop='wrap']");
			for(var i=0; i < element.size(); i++){
				new SelectBox(element[i]);
			}
		});
		/****** Module SelectBox only use Javascript ******/
		//require(["SelectBox"]);
		/****** Module Popup with Jquery ******/
		require(["$ModulePop"]);
		/****** Module Popup only use Javascript ******/
		//require(["ModulePop"]);
        /****** Module tab only use Javascript ******/
        /*require(["tab"], function(tab){
            var element = document.querySelectorAll("[data-module-tab='wrap']");
            for(var i=0; i<element.length; i++){
                new tab(element[i]);
            }
        });*/
        /****** Module Tab with Jquery ******/
        require(["$tab"],function(tab){
            var element = $("[data-module-tab='wrap']");
            for(var i=0; i<element.size();i++){
                new tab(element[i]);
            }
        });
        /****** Module Only put number with Jquery ******/
        require(["$number"],function(dataType){
            var element= $("[data-text-type='number']");
            for(var i=0;i<element.size();i++){
                new dataType(element[i]);
            }
        });
        /****** Module Move Top with Jquery ******/
        require(["$moveTop"]);
    }
);
