define([],function () {
		var tab = function(options){
			this.wrap=$(options);
			this.btnWrap = $("[data-module-tabBtn='wrap']",options);
			this.btnList= $(">li", this.btnWrap);
			this.btn= $(">button", this.btnList);
			this.tabList = $("[data-module-tab='contentsList']",options);
			this.oldTemp=0;
			this.newTemp;

			this.bindEvents();

		}
		tab.prototype={
			bindEvents:function(){
				var that = this;
				this.btn.on("click", function(){
					that.activeEvent($(this));
				})
			},
			activeEvent:function(me){
				this.newTemp = me.parent().index();
				this.btnList.eq(this.oldTemp).removeClass("on");
				this.btnList.eq(this.newTemp).addClass("on");
				this.tabList.eq(this.oldTemp).removeClass("on");
				this.tabList.eq(this.newTemp).addClass("on");
				this.oldTemp = this.newTemp;
			}
		}
		return tab;
    }
);
