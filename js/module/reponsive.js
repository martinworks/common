define([],function () {
		var tabletSize = 1024,
			mobileSize = 640;
		var responsive ={
			init : function(){
				this.size = Math.max( document.documentElement.clientWidth | window.innerWidth | window.availWidth);
				this.resolution;
				this.oldSize;
				this.newSize;

				//start function
				this.checkSize();
				this.callSize();
			},
			checkSize:function(){
				this.resolution = this.size;
				if(this.resolution > tabletSize ){
					this.resolution = "PC";
				} else if ( (this.resolution <= tabletSize) && (this.resolution > mobileSize) ) {
					this.resolution="Tablet";
				} else if ( this.resolution <= mobileSize){
					this.resolution="Mobile";
				}
			},
			callSize:function(){
				this.newSize = this.resolution;
				if(this.newSize !== this.oldSize){
					this.oldSize = this.newSize;
					this.currentSize = this.oldSize;
					//Check Device size;
					//console.log(this.currentSize);
				}

			},
			currentSize : ""
		}
		responsive.init();
		$(window).resize(function(){
			responsive.init();
		})
		return responsive;
    }
);
