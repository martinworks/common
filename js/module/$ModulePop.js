define([],function () {
		var cnt=-1;	//Popup default size
		var moudePopBtn = {
			init:function(){
				this.openBtn=$("[data-module-btn]");
				this.$d = $("body");

				this.bindEvents();
			},
			bindEvents:function(){
				var that = this;

				this.openBtn.unbind("click").on("click",function(){
					that.appendPop($(this).attr("data-module-btn"));
				})
			},
			appendPop:function(flag){
				var file = flag,
						that = this;
				$.ajax({
						type : "GET",
						url : file + ".html",
						dataType : "html",
						success : function(data){
							that.$d.append(data);
							that.init();
							new modulePop("[data-module-pop='popup']", flag);
							cnt++;
						},
						error:function(){
							console.log("Data loading false")
						}
				})

			}

		}
		var modulePop = function(options, flag){
			var that = this;
			this.$d = $("body");
			this.popContents=$(options);
			this.closeBtn=$(".close_btn", options);
			this.popTitle = $(".pop__wrap__contents__title", options);
			this.popParagraph =$(".pop__wrap__contents__paragraph", options);
			this.bindEvents(flag);
		}
		modulePop.prototype={
			bindEvents:function(flag){
				this.temp = flag;
				var that = this;
				this.closeBtn.unbind("click").on("click",function(e){
					e.stopPropagation();
					that.close();
					cnt--;
				})
				$(window).unbind("keydown").keydown( function(e){
					if(cnt >= 0){
						e.which == 27 ? that.close() : false;
						cnt--;
					}
				})
			},
			close:function(){
				cnt <= -1 ? false : this.popContents[cnt].remove();
			}
		}
		return moudePopBtn.init();
});
