define(['responsive'],function (SIZE){
	var SelectBox = function(options){
			this.wrap =$(options);
			this.btn=$(">button", this.wrap);
			this.selectListWrap = $(">ul",this.wrap);
			this.selectList = $(">li >a",this.selectListWrap);
			this.mobileSelectWrap = $("select", this.wrap);
			this.divideEvents();
		};

		SelectBox.prototype = {
			divideEvents:function(){
				//Devide Device resolution
				var that = this;
				var NEW_SIZE=SIZE.currentSize;
				$(window).resize(function(){
					OLD_SIZE = SIZE.currentSize;
					if( OLD_SIZE !== NEW_SIZE ){
						NEW_SIZE = OLD_SIZE;
						that.bindEvents(NEW_SIZE);
					}
				})
				this.bindEvents(NEW_SIZE);
			},
			bindEvents:function(resolution) {
				var that =this;
				//PC Event
				this.btn.unbind("click").on("click", function(){
					resolution == "PC" ? that.pcToggle(): false;
				})
				this.selectList.on("mouseenter", function(event){
					resolution == "PC" ? that.pcHover($(this)): false;
				})
				this.selectList.on("click", function(){
					resolution == "PC" ? that.pcHover($(this), "clickEvt") : false;
				})
				//Mobile and Tablet Event
				resolution !== "PC" ? this.selectListWrap.hide() : false;
				this.mobileSelectWrap.unbind("change").on("change", function(){
					resolution !== "PC" ? that.mobileSelect( $(this)) : false;
				})
			},
			pcToggle:function(){
				if(this.selectListWrap.is(":visible")){
					this.selectListWrap.slideUp("fast");
				}else{
					this.selectListWrap.slideDown("fast");
				}
			},
			pcHover: function(me, evt){
				this.btn.text(me.text())
				evt=="clickEvt" ? this.pcToggle() : false;
			},
			mobileSelect:function(me){
				this.btn.text(me.find("option:selected").text());
			}
		};

		return SelectBox;
	}
);
