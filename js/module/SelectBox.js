define([],function () {
		var TabInit = document.querySelectorAll("[data-module-pop='wrap']")
		function moduleDropDownTab(obj, evt){
			var element = obj;
			function Tab(){}
			Tab.prototype.init=function(obj){
				var temp = true;
				var that =this;
				this.wrap = obj;
				this.openBtn = obj.querySelector(".module_select_box__btn");
				this.selectListWrap = obj.querySelector(".module_select_box__list_wrap");
				this.selectList = this.selectListWrap.querySelectorAll("li");
				var originClass = this.selectListWrap.getAttribute("class");

				//binding event
				this.openBtn.addEventListener("click", offList);
				for(var  u=0; u<this.selectList.length;u++){
					var selectBtn = this.selectList[u].querySelector("a");
					selectBtn.addEventListener("click", clickEvent);
				}

				//event
				function clickEvent(){
					that.openBtn.innerHTML = this.innerHTML;
					offList();
				}
				function offList(){
					if(temp){
						that.selectListWrap.className +=" on";
						temp= false;
					} else{
						that.selectListWrap.setAttribute("class", originClass);
						temp = true;
					}
				}
			}
			for(var i=0; i < element.length; i++){
				instance = new Tab();
				instance.init(element[i]);
			}
		}
		return moduleDropDownTab(TabInit);
    }
);