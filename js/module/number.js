define([],function () {
		var dataType =function(options){
			this.textArea=$(options);
			this.bindEvents();

		}
		dataType.prototype={
			bindEvents:function(){
				var regNum = /^[0-9]*$/;
				var that=this;
				var onlyNum=""
				this.textArea.unbind("keydown").on("keydown",function(evt){
					//console.log(evt.which)
					if( !regNum.test($(this).val()) ){
						that.keyEvents(evt,onlyNum);
					}
					onlyNum=$(this).val();
				})
			},
			keyEvents:function(evt, value){
				//number keys 48-57 96-105, backspace key 8, delete key 46, arrow keys 37-40, ctrl key 17

				if( (evt.which ==8) || (evt.which==17) || (evt.which==46) || (evt.which>=37 && evt.which<=40)){
					console.log("key board press");
				} else {
					console.log(value)
					this.textArea.val(value);
					evt.preventDefault();
					return false;
				}
			}
		}
		return dataType;
    }
);
