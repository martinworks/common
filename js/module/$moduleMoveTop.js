define([],function (){
	var moveTop = {
		init:function(){
			this.btn=$("[data-module-button='moveTop']");
			this.$d= $(document);
			this.d =document;
			this.$b=$("body");
			this.footer=$(".footer");
			this.showSection = this.$d.scrollTop();
			this.limit =  this.$b.innerHeight() -this.footer.innerHeight();

			this.bindEvents();
			this.showBtn();
			this.fixBtn();
		},
		bindEvents:function(){
			var that = this;
			$(window).on("scroll",function(){
				that.showSection = that.$d.scrollTop();
				//Show function
				that.showBtn();
				//Fix function
				that.fixBtn();
			})
			this.btn.on("click", function(){
				that.move();
			})
		},
		showBtn:function(){
			if(this.$d.scrollTop() > 100){
				this.btn.fadeIn();
			} else{
				this.btn.fadeOut();
			}
		},
		move:function(){
			this.$b.stop().animate({
				"scrollTop" : 0
			},{
				easing : "easeOutQuart",
				duration:500
			})
		},
		fixBtn:function(){
		}
	}
	return moveTop.init();
});
