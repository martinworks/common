define([],function () {
		var modulePop = {
			init : function(){
				var that = this;
				this.openBtn = document.querySelectorAll("[data-module-btn]");
				this.popContents= document.querySelectorAll("[data-module-pop]");

				//function start
				this.func();
			},
			func : function(){
				var that = this,
				     temp,
				     cnt,
				     escKey,
				     innerContents,
				     closeBtn;
				//function binding
				for(var i =0; i<this.openBtn.length; i++){
					this.openBtn[i].addEventListener("click", openPopup );
				}
				for(var o =0; o<this.popContents.length; o++){
					innerContents = this.popContents[o].querySelector(".pop__wrap__contents");
					closeBtn = innerContents.querySelector(".close_btn");

					this.popContents[o].addEventListener("click", ClosePopup );
					closeBtn.addEventListener("click", ClosePopup);
					innerContents.addEventListener("click", stopBubblingEffect);
				}

				window.onkeydown = function(event){
					escKey = event.keyCode | event.which;
					escKey==27 ? ClosePopup() : false;
				}
				
				function openPopup(){
					temp =this.getAttribute("data-module-btn");
					for(var u=0; u<that.popContents.length; u++  ){
						if(that.popContents[u].getAttribute("data-module-pop") == temp){
							cnt =u;
						}
					}
					that.popContents[cnt].style.display = "block"
				}

				function ClosePopup(){
					that.popContents[cnt].style.display = "none"
				}

				function stopBubblingEffect(event){
					event.stopPropagation();
				}
				
			}
			
		}
		return modulePop.init();

    }
);